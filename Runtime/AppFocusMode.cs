﻿namespace com.FDT.SystemEventsHandler
{
    /// <summary>
    /// Creation Date:   28/02/2020 17:35:21
    /// Product Name:    FDT System Events Handler
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public enum AppFocusMode
    {
        FOCUSED = 0, NOT_FOCUSED = 1, PAUSED = 2
    }
}