﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler
{ 
    /// <summary>
    /// Creation Date:   28/02/2020 17:16:12
    /// Product Name:    FDT System Events Handler
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:       --/--/-- - Changed GameEvents to UnityEvents to avoid dependence.
    /// </summary>
    public class SystemEventsHandler : MonoBehaviour 
    {
        #region Classes, Structs and Enums
        [System.Serializable]
        public class SceneLoadUEvent : UnityEvent<Scene, LoadSceneMode> {}
        [System.Serializable]
        public class SceneUnloadUEvent : UnityEvent<Scene> {}
        [System.Serializable]
        public class ActiveSceneChangedUEvent : UnityEvent<Scene, Scene> {}
        [System.Serializable]
        public class FocusModeChangedUEvent : UnityEvent<AppFocusMode> {}
        [System.Serializable]
        public class ScreenOrientationChangeUEvent : UnityEvent<ScreenOrientation> {}
        
        #endregion
        #region GameEvents and UnityEvents
        [Header("Application"), SerializeField] private UnityEvent _lowMemoryEvt;
        [SerializeField] private UnityEvent _quittingEvt;
        [Header("SceneManager"), SerializeField] private SceneLoadUEvent _loadSceneEvt;
        [SerializeField] private SceneUnloadUEvent _unloadSceneEvt;
        [SerializeField] private ActiveSceneChangedUEvent _activeSceneChangedEvt;
        [Header("Focus and screen"), SerializeField] private FocusModeChangedUEvent _focusModeChangedEvt;
        [SerializeField] private ScreenOrientationChangeUEvent _screenOrientationEvt;
        #endregion

        #region Inspector Fields
        [SerializeField] private AppFocusMode _focus = AppFocusMode.FOCUSED;
        [SerializeField] private ScreenOrientation _orientation;
        #endregion

        #region Variables

        private bool paused = false;
        private bool focused = true;
        #endregion

        #region Methods

        private void Awake () {
            Application.lowMemory += HandleLowMemory;
            Application.quitting += HandleQuitting;
            SceneManager.sceneLoaded += HandleSceneLoaded;
            SceneManager.sceneUnloaded += HandleSceneUnloaded;
            SceneManager.activeSceneChanged += HandleActiveSceneChanged;
        }

        private void Update()
        {
            if (Screen.orientation == _orientation) return;
            _orientation = Screen.orientation;
            _screenOrientationEvt.Invoke(_orientation);
        }
        private void OnApplicationFocus(bool focusStatus)
        {
            if (focused != focusStatus)
            { 
                focused = focusStatus;
                SetFocusMode();
            }
            Debug.Log("OnApplicationFocus " + focusStatus);
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (paused != pauseStatus)
            { 
                paused = pauseStatus;
                SetFocusMode();
            }
            Debug.Log("OnApplicationPause " + pauseStatus);
        }

        private void SetFocusMode()
        {
            bool changed = false;
            if (focused && !paused)
            {
                if (_focus != AppFocusMode.FOCUSED)
                {
                    changed = true;
                    _focus = AppFocusMode.FOCUSED;
                }
            }
            else if (!focused && !paused)
            { 
                if (_focus != AppFocusMode.NOT_FOCUSED)
                {
                    changed = true;
                    _focus = AppFocusMode.NOT_FOCUSED;
                }
            }
            else if (paused)
            { 
                if (_focus != AppFocusMode.PAUSED)
                {
                    changed = true;
                    _focus = AppFocusMode.PAUSED;
                }
            }
            if (changed)
                _focusModeChangedEvt.Invoke(_focus);
        }

        private void HandleActiveSceneChanged(Scene current, Scene next)
        {
            _activeSceneChangedEvt?.Invoke(current, next);
        }

        private void HandleSceneLoaded(Scene s, LoadSceneMode m)
        {
            _loadSceneEvt?.Invoke(s, m);
        }

        private void HandleSceneUnloaded(Scene s)
        {
            _unloadSceneEvt?.Invoke(s);
        }

        private void HandleLowMemory()
        {
            _lowMemoryEvt?.Invoke();
        }

        private void HandleQuitting()
        {
            _quittingEvt?.Invoke();
        }

        private void OnDestroy () {
            Application.quitting -= HandleQuitting;
            SceneManager.activeSceneChanged -= HandleActiveSceneChanged;
            SceneManager.sceneUnloaded -= HandleSceneUnloaded;
            SceneManager.sceneLoaded -= HandleSceneLoaded;
            Application.lowMemory -= HandleLowMemory;
        }
        #endregion
    }
}